# Конфигурация Docker Compose для JBPM, NGINX, Grafana, Prometheus и Node Exporter

Эта конфигурация Docker Compose настраивает среду с несколькими контейнерами для запуска JBPM, NGINX, Grafana, Prometheus и Node Exporter. Она предоставляет базовую настройку для этих сервисов и может быть настроена в соответствии с вашими требованиями.

## Предварительные требования

- Установленный Docker на вашем компьютере.
- Установленный Docker Compose на вашем компьютере.

## Использование

1. Клонируйте этот репозиторий:

   ```shell
   git clone git@gitlab.com:agilev90/jbpm-grafana.git
   cd jbpm-grafana
   ```

2. Запустите сервисы и дождитесь инициализации

   ```shell
   docker compose up -d
   ```

3. Доступ к сервисам:

- JBPM: Доступ к серверу JBPM по адресу <http://localhost>. Можете перейти на <http://localhost/business-central> чтобы попать в Business central
- Grafana: Доступ к Grafana по адресу <http://localhost/grafana>. Используйте логин **admin** и пароль **test**. По умолчанию подключен dashboard.

## License

   This project is licensed under the MIT License. See the LICENSE file for details.
